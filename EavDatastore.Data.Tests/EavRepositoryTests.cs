using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EavDatastore.Data.Enums;
using EavDatastore.Data.Repositories;
using EavDatastore.Data.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Xunit;

namespace EavDatastore.Data.Tests
{
    public class EavRepositoryTests
    {
        [Fact]
        public async Task Attribute_change_tracking_works_correctly_on_create()
        {
            //Arrange
            const string sectionName = "Section name";
            
            var eavDbContext = GetContextHelper("db");

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var eavRepository = new EavRepository(eavDbContext, memoryCache);

            var attr1 = new DbAttribute
            {
                Name = sectionName
            };

            var val1 = new DbValue
            {
                Attribute = attr1,
                TextValue = "Box"
            };

            var entity1 = new DbEntity
            {
                Type = EntityType.SportSection,
                Values = new List<DbValue> { val1 }
            };

            //Act 1: add when no attributes are presented in database

            await eavRepository.CreateAsync(entity1);
            
            //Assert 1: all hierarchy is added successfully

            var entity1Retrieved = eavDbContext.Entities.Single();
            Assert.Equal(EntityType.SportSection, entity1Retrieved.Type);
            Assert.NotEqual(Guid.Empty, entity1Retrieved.Id);
            
            var attribute1Retrieved = eavDbContext.Attributes.Single();
            Assert.NotEqual(Guid.Empty, attribute1Retrieved.Id);
            Assert.Equal(attr1.Name, attribute1Retrieved.Name);

            var value1Retrieved = eavDbContext.Values.Single();
            Assert.NotEqual(Guid.Empty, value1Retrieved.Id);
            Assert.Equal(val1.TextValue, value1Retrieved.TextValue);
            Assert.Equal(attribute1Retrieved.Id, value1Retrieved.AttributeId);
            Assert.Equal(entity1Retrieved.Id, value1Retrieved.EntityId);

            Assert.Equal(0, memoryCache.Count);
            
            //Arrange 2:
            //Cleanup tracking context
            eavDbContext = GetContextHelper("db");
            eavRepository = new EavRepository(eavDbContext, memoryCache);
            
            var attr2 = new DbAttribute
            {
                Name = sectionName
            };

            var val2 = new DbValue
            {
                Attribute = attr2,
                TextValue = "Karate"
            };

            var entity2 = new DbEntity
            {
                Type = EntityType.SportSection,
                Values = new List<DbValue> { val2 }
            };
            
            //Act 2: add when attribute "Name" is already presented in database and will be cached
            
            await eavRepository.CreateAsync(entity2);

            Assert.Equal(1, memoryCache.Count);
            var sectionNameAttribute = memoryCache.Get<DbAttribute>(sectionName);
            Assert.NotNull(sectionNameAttribute);
            
            Assert.Equal(2, eavDbContext.Entities.Count());
            Assert.Equal(1, eavDbContext.Attributes.Count());
            Assert.Equal(2, eavDbContext.Values.Count());
        }
        
        [Fact]
        public async Task Attribute_change_tracking_works_correctly_on_update()
        {
            //Arrange
            const string sectionName = "Section name";
            
            var eavDbContext = GetContextHelper("db");

            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var eavRepository = new EavRepository(eavDbContext, memoryCache);

            var attributeId = Guid.NewGuid();
            var attr1 = new DbAttribute
            {
                Id = attributeId,
                Name = sectionName
            };
            
            var valueId = Guid.NewGuid();
            var val1 = new DbValue
            {
                Id = valueId,
                Attribute = attr1,
                TextValue = "Box"
            };

            var entityId = Guid.NewGuid();
            var entity1 = new DbEntity
            {
                Id = entityId,
                Type = EntityType.SportSection,
                Values = new List<DbValue> { val1 }
            };

            await eavRepository.CreateAsync(entity1);
            
            //Cleanup tracking context
            eavDbContext = GetContextHelper("db");
            eavRepository = new EavRepository(eavDbContext, memoryCache);
            
            var attrUpdated = new DbAttribute
            {
                Id = attributeId,
                Name = sectionName
            };
            
            var valUpdated = new DbValue
            {
                Id = valueId,
                Attribute = attrUpdated,
                TextValue = "Karate"
            };

            var entityUpdated = new DbEntity
            {
                Id = entityId,
                Type = EntityType.SportSection,
                Values = new List<DbValue> { valUpdated }
            };
            
            //Act
            
            await eavRepository.UpdateAsync(entityUpdated);
            
            //Cleanup tracking context
            eavDbContext = GetContextHelper("db");
            eavRepository = new EavRepository(eavDbContext, memoryCache);

            var retrievedEntity = await eavRepository.GetByIdAsync(entityId);
            
            Assert.Equal("Karate", retrievedEntity.Values.First().TextValue);
            
            Assert.Equal(1, eavDbContext.Entities.Count());
            Assert.Equal(1, eavDbContext.Attributes.Count());
            Assert.Equal(1, eavDbContext.Values.Count());
        }

        [Fact]
        public async Task GeyById_works_correctly()
        {
            //Arrange
            var dbContext = GetContextHelper("db1");
            //Create base data
            const string attributeName = "Section starts at";
            var attr1 = new DbAttribute
            {
                Name = attributeName
            };

            const string textValue = "Karate";
            var val1 = new DbValue
            {
                Attribute = attr1,
                TextValue = textValue
            };

            var entityId = Guid.NewGuid();
            var entity1 = new DbEntity
            {
                Id = entityId,
                Type = EntityType.SportSection,
                Values = new List<DbValue> { val1 }
            };
            
            dbContext.Add(entity1);
            dbContext.SaveChanges();
            
            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var eavRepository = new EavRepository(GetContextHelper("db1"), memoryCache);
            
            //Act

            var dbEntity = await eavRepository.GetByIdAsync(entityId);
            
            //Assert
            
            Assert.NotNull(dbEntity);
            Assert.NotNull(dbEntity.Values);
            Assert.Equal(entityId, dbEntity.Id);
            Assert.Equal(1, dbEntity.Values.Count);
            
            var dbValue = dbEntity.Values.First();
            Assert.Equal(entityId, dbValue.EntityId);
            Assert.Equal(textValue, dbValue.TextValue);
            
            Assert.NotNull(dbValue.Attribute);
            Assert.Equal(attributeName, dbValue.Attribute.Name);
        }

        [Fact]
        public async Task DeleteById_works_correctly()
        {
            //Arrange
            var dbContext = GetContextHelper("db1");
            //Create base data
            const string attributeName = "Section starts at";
            var attr1 = new DbAttribute
            {
                Name = attributeName
            };

            const string textValue = "Karate";
            var val1 = new DbValue
            {
                Attribute = attr1,
                TextValue = textValue
            };

            var entityId = Guid.NewGuid();
            var entity1 = new DbEntity
            {
                Id = entityId,
                Type = EntityType.SportSection,
                Values = new List<DbValue> { val1 }
            };
            
            dbContext.Add(entity1);
            dbContext.SaveChanges();
            
            var memoryCache = new MemoryCache(new MemoryCacheOptions());

            var eavRepository = new EavRepository(GetContextHelper("db1"), memoryCache);
            
            //Act

            await eavRepository.DeleteByIdAsync(entityId);
            
            //Assert that cascade deletion was done for entity and value
            
            var retrieved = await eavRepository.GetByIdAsync(entityId);
            
            Assert.Null(retrieved);
        }
        
        [Fact]
        public async Task Update_works_correctly()
        {
            //ToDo: write this test
        }

        /// <summary>
        /// Creates new EavDbContext. Could also be used to recreate the scope during the unit tests 
        /// </summary>
        private static EavDbContext GetContextHelper(string dbName = null)
        {
            var dbContext = new EavDbContext(new DbContextOptionsBuilder<EavDbContext>()
                .UseInMemoryDatabase(dbName ?? Guid.NewGuid().ToString())
                .Options
            );

            return dbContext;
        }
    }
}