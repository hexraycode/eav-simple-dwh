using System;

namespace EavDatastore.Api.ViewModels
{
    public class ClubParticipant
    {
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public string Age { get; set; }
    }
}