using System;
using System.Threading.Tasks;
using EavDatastore.Api.Services.Interfaces;
using EavDatastore.Api.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace EavDatastore.Api.Controllers
{
    [Route("api/club/participant")]
    public class ClubParticipantsController : Controller
    {
        private readonly IParticipantService _participantService;

        public ClubParticipantsController(IParticipantService participantService)
        {
            _participantService = participantService ?? throw new ArgumentNullException(nameof(participantService));
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateParticipant([FromBody] ClubParticipant participant)
        {
            await _participantService.InsertParticipantAsync(participant);

            return Ok();
        }
        
        [HttpGet("id")]
        public async Task<IActionResult> GetParticipant(Guid id)
        {
            var result = await _participantService.GetParticipantAsync(id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
        
        [HttpDelete("id")]
        public async Task<IActionResult> DeleteParticipant(Guid id)
        {
            await _participantService.DeleteParticipantAsync(id);
            return Ok();
        }
        
        [HttpPut]
        public async Task<IActionResult> UpdateParticipant([FromBody] ClubParticipant participant)
        {
            await _participantService.UpdateParticipantAsync(participant);
            return Ok();
        }
    }
}