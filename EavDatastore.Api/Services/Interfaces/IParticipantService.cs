using System;
using System.Threading.Tasks;
using EavDatastore.Api.ViewModels;

namespace EavDatastore.Api.Services.Interfaces
{
    public interface IParticipantService
    {
        Task InsertParticipantAsync(ClubParticipant clubParticipant);

        Task<ClubParticipant> GetParticipantAsync(Guid id);

        Task DeleteParticipantAsync(Guid id);

        Task UpdateParticipantAsync(ClubParticipant clubParticipant);
    }
}