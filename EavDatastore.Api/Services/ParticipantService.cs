using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EavDatastore.Api.Services.Interfaces;
using EavDatastore.Api.ViewModels;
using EavDatastore.Data.Enums;
using EavDatastore.Data.Repositories;
using EavDatastore.Data.Schema;

namespace EavDatastore.Api.Services
{
    public class ParticipantService : IParticipantService
    {
        private readonly IEavRepository _repository;
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";
        private const string Age = "Age";

        public ParticipantService(IEavRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task InsertParticipantAsync(ClubParticipant clubParticipant)
        {
            clubParticipant.Id = Guid.NewGuid();
            var participantEntity = ToEntity(clubParticipant);
            await _repository.CreateAsync(participantEntity);
        }

        public async Task<ClubParticipant> GetParticipantAsync(Guid id)
        {
            var participantEntity = await _repository.GetByIdAsync(id);
            var participantModel = ToModel(participantEntity);
            
            return participantModel;
        }

        public async Task DeleteParticipantAsync(Guid id)
        {
            await _repository.DeleteByIdAsync(id);
        }

        public async Task UpdateParticipantAsync(ClubParticipant clubParticipant)
        {
            await _repository.DeleteByIdAsync(clubParticipant.Id);
            
            var participantEntity = ToEntity(clubParticipant);
            await _repository.CreateAsync(participantEntity);
        }

        private ClubParticipant ToModel(DbEntity dbEntity)
        {
            var clubParticipant = new ClubParticipant
            {
                Id = dbEntity.Id,
                FirstName = dbEntity.Values.First(v => v.Attribute.Name == FirstName).TextValue,
                LastName = dbEntity.Values.First(v => v.Attribute.Name == LastName).TextValue,
                Age = dbEntity.Values.First(v => v.Attribute.Name == Age).TextValue
            };

            return clubParticipant;
        }
        
        private DbEntity ToEntity(ClubParticipant participant)
        {
            var dict = new Dictionary<string, string>
            {
                {FirstName, participant.FirstName},
                {LastName, participant.LastName},
                {Age, participant.Age}
            };

            var values = new List<DbValue>();

            foreach (var kvp in dict)
            {
                values.Add(new DbValue
                {
                    Attribute = new DbAttribute { Name = kvp.Key },
                    TextValue = kvp.Value
                });
            }

            var dbEntity = new DbEntity
            {
                Id = participant.Id,
                Type = EntityType.ClubParticipant,
                Values = values
            };

            return dbEntity;
        }
    }
}