using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EavDatastore.Api.Services;
using EavDatastore.Api.ViewModels;
using EavDatastore.Data.Enums;
using EavDatastore.Data.Repositories;
using EavDatastore.Data.Schema;
using Moq;
using Xunit;

namespace EavDatastore.Api.Tests
{
    public class ParticipantServiceTests
    {
        [Fact]
        public async Task Create_Works_Correctly()
        {
            //Arrange
            var eavRepositoryMock = new Mock<IEavRepository>();

            DbEntity passedArgEntity = null;
            eavRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<DbEntity>()))
                .Callback<DbEntity>(argEntity => passedArgEntity = argEntity)
                .Returns(Task.CompletedTask);

            var participantService = new ParticipantService(eavRepositoryMock.Object);

            var clubParticipant = new ClubParticipant
            {
                Age = "17",
                FirstName = "John",
                LastName = "Johnson"
            };
            
            //Act
            await participantService.InsertParticipantAsync(clubParticipant);
            
            //Assert
            eavRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<DbEntity>()), Times.Once);
            Assert.NotNull(passedArgEntity);
            
            Assert.NotEqual(Guid.Empty, passedArgEntity.Id);
            Assert.Equal(EntityType.ClubParticipant, passedArgEntity.Type);

            var fn = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("FirstName"));
            var ln = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("LastName"));
            var age = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("Age"));
            Assert.Equal("John", fn.TextValue);
            Assert.Equal("Johnson", ln.TextValue);
            Assert.Equal("17", age.TextValue);
        }

        [Fact]
        public async Task Delete_works_correctly()
        {
            //Arrange
            var eavRepositoryMock = new Mock<IEavRepository>();
            var participantService = new ParticipantService(eavRepositoryMock.Object);

            var participantId = Guid.NewGuid();
            
            //Act
            await participantService.DeleteParticipantAsync(participantId);

            //Assert
            eavRepositoryMock.Verify(x => x.DeleteByIdAsync(participantId), Times.Once);
        }

        [Fact]
        public async Task Get_works_correctly()
        {
            //Arrange
            var eavRepositoryMock = new Mock<IEavRepository>();

            var dbValueFn = CreatePair("FirstName", "John");
            var dbValueLn = CreatePair("LastName", "Johnson");
            var dbValueAge = CreatePair("Age", "42");
            
            var dbEntity = new DbEntity
            {
                Id = Guid.NewGuid(),
                Type = EntityType.ClubParticipant,
                Values = new List<DbValue>{ dbValueFn, dbValueLn, dbValueAge }
            };

            eavRepositoryMock.Setup(x => x.GetByIdAsync(dbEntity.Id))
                .Returns(Task.FromResult(dbEntity));
            
            var participantService = new ParticipantService(eavRepositoryMock.Object);
            
            //Act
            var participant = await participantService.GetParticipantAsync(dbEntity.Id);

            //Assert
            
            Assert.Equal(dbEntity.Id, participant.Id);
            Assert.Equal("John", participant.FirstName);
            Assert.Equal("Johnson", participant.LastName);
            Assert.Equal("42", participant.Age);
        }
        
        [Fact]
        public async Task Update_Works_Correctly()
        {
            //Arrange
            var eavRepositoryMock = new Mock<IEavRepository>();

            DbEntity passedArgEntity = null;
            eavRepositoryMock.Setup(x => x.CreateAsync(It.IsAny<DbEntity>()))
                .Callback<DbEntity>(argEntity => passedArgEntity = argEntity)
                .Returns(Task.CompletedTask);

            var participantService = new ParticipantService(eavRepositoryMock.Object);

            var clubParticipant = new ClubParticipant
            {
                Id = Guid.NewGuid(),
                Age = "17",
                FirstName = "John",
                LastName = "Johnson"
            };
            
            //Act
            await participantService.UpdateParticipantAsync(clubParticipant);
            
            //Assert
            eavRepositoryMock.Verify(x => x.DeleteByIdAsync(clubParticipant.Id), Times.Once);
            
            eavRepositoryMock.Verify(x => x.CreateAsync(It.IsAny<DbEntity>()), Times.Once);
            Assert.NotNull(passedArgEntity);
            
            Assert.NotEqual(Guid.Empty, passedArgEntity.Id);
            Assert.Equal(EntityType.ClubParticipant, passedArgEntity.Type);

            var fn = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("FirstName"));
            var ln = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("LastName"));
            var age = passedArgEntity.Values.First(v => v.Attribute.Name.Equals("Age"));
            Assert.Equal("John", fn.TextValue);
            Assert.Equal("Johnson", ln.TextValue);
            Assert.Equal("17", age.TextValue);
        }

        private DbValue CreatePair(string name, string value)
        {
            var dbAttribute = new DbAttribute
            {
                Id = Guid.NewGuid(),
                Name = name
            };

            var dbValue = new DbValue
            {
                Id = Guid.NewGuid(),
                Attribute = dbAttribute,
                TextValue = value
            };

            return dbValue;
        }
    }
}