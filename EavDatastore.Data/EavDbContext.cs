using System;
using EavDatastore.Data.Schema;
using Microsoft.EntityFrameworkCore;

namespace EavDatastore.Data
{
    public class EavDbContext : DbContext
    {
        public DbSet<DbEntity> Entities { get; set; }
        
        public DbSet<DbAttribute> Attributes { get; set; }
        
        public DbSet<DbValue> Values { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbAttribute>().HasIndex(att => att.Name).IsUnique();

            modelBuilder.Entity<DbAttribute>()
                .HasMany(attr => attr.Values)
                .WithOne(val => val.Attribute)
                .HasForeignKey(val => val.AttributeId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DbEntity>()
                .HasMany(ent => ent.Values)
                .WithOne(val => val.Entity)
                .HasForeignKey(val => val.EntityId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

        }

        public EavDbContext(DbContextOptions<EavDbContext> options) : base(options)
        {
        }
    }
}