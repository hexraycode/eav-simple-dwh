namespace EavDatastore.Data.Enums
{
    // Can be replaced by another table in case we'll have many entities 
    
    public enum EntityType
    {
        Unknown = 0,
        MedPatient = 1,
        MedOperation = 2,
        ClubParticipant = 3,
        SportSection = 4
    }
}