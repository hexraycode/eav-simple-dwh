using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EavDatastore.Data.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace EavDatastore.Data.Repositories
{
    public class EavRepository : IEavRepository
    {
        private readonly EavDbContext _dbContext;
        private readonly IMemoryCache _memoryCache;

        public EavRepository(EavDbContext dbContext, IMemoryCache memoryCache)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));
        }
        
        public async Task CreateAsync(DbEntity dbEntity)
        {
            await ApplyAttributesChangeTracking(dbEntity);
            _dbContext.Entities.Add(dbEntity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<DbEntity> GetByIdAsync(Guid entityId)
        {
            var retrievedEntity = await _dbContext.Entities
                .Include(entity => entity.Values)
                    .ThenInclude(val => val.Attribute)
                .Where(ent => ent.Id.Equals(entityId))
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return retrievedEntity;
        }

        public async Task UpdateAsync(DbEntity dbEntity)
        {
            await ApplyAttributesChangeTracking(dbEntity);
            _dbContext.Entities.Attach(dbEntity).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(Guid entityId)
        {
            //var entity = new DbEntity { Id = entityId };
            //_dbContext.Entities.Attach(entity);
            var entity = await _dbContext.Entities.FindAsync(entityId);
            _dbContext.Entities.Remove(entity);
            
            await _dbContext.SaveChangesAsync();
        }
        
        private async Task ApplyAttributesChangeTracking(DbEntity dbEntity)
        {
            if (dbEntity.Values == null || dbEntity.Values.Count == 0)
            {
                return;
            }
            
            var nonCachedAttributes = new List<string>();

            foreach (var entityValue in dbEntity.Values)
            {
                //If we cached an Attribute -> we already have it in the database
                //And we don't need to insert an Attribute together with the Value
                if (_memoryCache.TryGetValue(entityValue.Attribute.Name, out DbAttribute cachedAttribute))
                {
                    entityValue.Attribute = cachedAttribute;
                    entityValue.AttributeId = cachedAttribute.Id;
                    _dbContext.Entry(entityValue.Attribute).State = EntityState.Unchanged;
                }
                //We didn't cache the Attribute but it can exists in the Database
                //Check it and cache it
                //Or prepare to insert an Attribute together with Value
                else
                {
                    nonCachedAttributes.Add(entityValue.Attribute.Name);
                }
            }

            if (nonCachedAttributes.Count == 0)
            {
                return;
            }

            var retrievedNonCachedAttributes = await _dbContext
                .Attributes
                .Where(attr => nonCachedAttributes.Contains(attr.Name))
                .AsNoTracking()
                .ToListAsync();
            
            //Place all non-cached attributes into the cache
            //And set Unchanged to prevent the insertion of the Attribute into the Db
            foreach (var nonCachedAttribute in retrievedNonCachedAttributes)
            {
                _memoryCache.Set(nonCachedAttribute.Name, nonCachedAttribute);

                foreach (var entityValue in dbEntity.Values)
                {
                    if (nonCachedAttribute.Name.Equals(entityValue.Attribute.Name))
                    {
                        entityValue.Attribute = nonCachedAttribute;
                        entityValue.AttributeId = nonCachedAttribute.Id;
                        _dbContext.Entry(entityValue.Attribute).State = EntityState.Unchanged;
                    }
                }
            }

            //Remove all attributes that were not in cache but already cached
            nonCachedAttributes.RemoveAll(nca => retrievedNonCachedAttributes.Any(rta => rta.Name.Equals(nca)));
            
            //If we have the attributes which are not presented neither in cache nor in database
            if (nonCachedAttributes.Count != 0)
            {
                //Instruct EF to insert these Attributes together with Values
                //So they will be retrieved in cache next time
                foreach (var entityValue in dbEntity.Values)
                {
                    if (nonCachedAttributes.Contains(entityValue.Attribute.Name))
                    {
                        _dbContext.Entry(entityValue.Attribute).State = EntityState.Added;
                    }
                }
            }
        }
    }
}