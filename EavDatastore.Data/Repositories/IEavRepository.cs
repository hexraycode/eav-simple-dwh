using System;
using System.Threading.Tasks;
using EavDatastore.Data.Schema;

namespace EavDatastore.Data.Repositories
{
    public interface IEavRepository
    {
        Task CreateAsync(DbEntity dbEntity);
        Task<DbEntity> GetByIdAsync(Guid entityId);
        Task UpdateAsync(DbEntity dbEntity);
        Task DeleteByIdAsync(Guid entityId);
    }
}