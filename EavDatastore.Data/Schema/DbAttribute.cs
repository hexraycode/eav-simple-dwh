using System;
using System.Collections.Generic;

namespace EavDatastore.Data.Schema
{
    public class DbAttribute
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public List<DbValue> Values { get; set; }
    }
}