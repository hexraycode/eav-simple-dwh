using System;
using System.Collections.Generic;
using EavDatastore.Data.Enums;

namespace EavDatastore.Data.Schema
{
    public class DbEntity
    {
        public Guid Id { get; set; }
        
        public EntityType Type { get; set; }
        
        public List<DbValue> Values { get; set; }
    }
}