using System;

namespace EavDatastore.Data.Schema
{
    public class DbValue
    {
        public Guid Id { get; set; }
        
        public Guid AttributeId { get; set; }
        
        public DbAttribute Attribute { get; set; }
        
        public Guid EntityId { get; set; }
        
        public DbEntity Entity { get; set; }
        
        public string TextValue { get; set; }
    }
}